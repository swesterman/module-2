<!DOCTYPE html>
<html>
<head>
<!-- idea to name radio buttons came from
   https://stackoverflow.com/questions/3216512/how-can-i-create-mutually-exclusive-radio-buttons-with-different-names#3216522
-->
   <title>Calculator</title>
   <style type="text/css">
      /* I just really like the Menlo font... I didn't want to make a whole seperate file just for this */
      body {
         font-family: "Menlo", Arial, sans-serif; 
      }
   </style>
</head>
<body>
   <?php 
      // if we have 3 things in the GET request (lhs, rhs, oper), then process the input
      if( sizeof($_GET) == 3 ) {
         $lhs =  $_GET['lhs'];
         $rhs =  $_GET['rhs'];
         $oper = $_GET['oper'];
         switch( $oper ) {
            case 'add':
               $lhs += $rhs; break;
            case 'sub':
               $lhs -= $rhs; break;
            case 'mul':
               $lhs *= $rhs; break;
            case 'div':
               if( $rhs == 0 ){
                  $lhs = "div by 0"; // we don't want to crash for our user, so pre-emptively catch divide by 0
               } else {
                  $lhs /= $rhs;
               }
               break;
            default:
               $lhs = "unknown oper `$oper`";
         }
      } else { // otherwise set the lefthand side to nothing
         $lhs = '';
      }
   ?>
   <form action="calculator.php">
      <!-- notice the lhs is set here via php -->
      <input type=text name=lhs value='<?php echo $lhs ?>'><br>
      <input type=text name=rhs><br>
      <input type=radio name=oper value="add">+
      <input type=radio name=oper value="sub">-
      <input type=radio name=oper value="mul">× <!-- this and div are special unicode characters -->
      <input type=radio name=oper value="div">÷<br>
      <input type=submit value="calculate!">
   </form>
</body>

</html>




